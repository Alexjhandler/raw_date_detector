require "test_helper"

class RawDateDetectorTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::RawDateDetector::VERSION
  end

  def test_raw_year
    assert RawDateDetector.raw_year?(1973)
  end

  def test_non_raw_year
    refute RawDateDetector.raw_year?(1999)
  end

  def test_string_date
    assert RawDateDetector.raw_year?('2020')
  end

  def test_year_arr
    year_arr = RawDateDetector.raw_arr(2020)
    assert_equal [2,0,2,0], year_arr
  end

end
