require_relative 'lib/raw_date_detector/version'

Gem::Specification.new do |spec|
  spec.name          = "raw_date_detector"
  spec.version       = RawDateDetector::VERSION
  spec.authors       = ["Alex Handler"]
  spec.email         = ["alexjhandler@gmail.com"]

  spec.summary       = %q{Detects raw life year dates}
  spec.description   = %q{detects and returns true or false for raw life year dates. a raw year is any year that when the first two digits added up devided by the last two digits added up equal 1. this is a silly gem}
  spec.homepage      = "https://gitlab.com/Alexjhandler/raw_date_detector"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org/"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/Alexjhandler/raw_date_detector"
  spec.metadata["changelog_uri"] = "https://gitlab.com/Alexjhandler/raw_date_detector"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
