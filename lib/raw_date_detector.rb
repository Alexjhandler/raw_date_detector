require "raw_date_detector/version"

module RawDateDetector

  def self.raw_arr(input)
    year_arr = input.to_s.split('').map { |digit| digit.to_i }
  end

  def self.raw_year?(input)
    year_arr = raw_arr(input)
    first_section = year_arr[0] + year_arr[1]
    second_section = year_arr[2] + year_arr[3]
    first_section / second_section == 1 ? true : false
  end

end
