# RawDateDetector

currently only works for years. This gem is a small bit of code to detect raw years. A raw year is any year where the first two digits added together divided by the last two digits added together equal 1. example:
**2020**
- `2 + 0 = 2`
- `2 + 0 = 2`
- `2 / 2 = 1`
> it's one life, the *raw life*
## Installation

Add this line to your application's Gemfile:

```ruby
gem 'raw_date_detector'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install raw_date_detector

## Usage
```ruby
RawDateDetector.raw_year?(input_year)
```

returns `true` for raw year and `false` for non raw year

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.


Currently, it only takes date. in development for other features


## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/Alexjhandler/raw_date_detector


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
